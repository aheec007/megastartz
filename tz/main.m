//
//  main.m
//  tz
//
//  Created by alex on 14.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
