//
//  Constants.h
//  tz
//
//  Created by alex on 16.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

static NSString *kBasePath = @"https://jsonplaceholder.typicode.com/";
static NSString *kPosts = @"posts";
static NSString *kComments = @"comments";

static NSString *kHTTPGET = @"GET";
static NSString *kStart = @"_start";
static NSString *kLimit = @"_limit";
static NSInteger kPageSize = 40;

static NSString *kPostCell = @"PostCell";
static NSString *kProgressCell = @"ProgressCell";
static NSString *kCommentCell = @"CommentCell";



#endif /* Constants_h */
