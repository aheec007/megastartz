//
//  ProgressCell.m
//  tz
//
//  Created by alex on 15.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import "ProgressCell.h"

@interface ProgressCell()
@property (weak, nonatomic) IBOutlet UIImageView *refrashImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@end

@implementation ProgressCell

- (void)setLoadingState:(State)state
{
    
    switch (state) {
        case ReadyToLoadingState:
            [self.spinner startAnimating];
            self.refrashImageView.hidden = YES;
            break;
            
        case ErrorState:
            self.refrashImageView.hidden = NO;
            self.refrashImageView.image = [UIImage imageNamed:@"warning"];
            [self.spinner stopAnimating];
            break;
            
        case LoadingState:
            self.refrashImageView.hidden = YES;
            [self.spinner startAnimating];
            break;
            
        case CompleteState:
            self.refrashImageView.hidden = NO;
            self.refrashImageView.image = [UIImage imageNamed:@"refrash"];
            [self.spinner stopAnimating];
            break;
            
        default:
            break;
    }
}


@end
