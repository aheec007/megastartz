//
//  ProgressCell.h
//  tz
//
//  Created by alex on 15.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, State){
    LoadingState = 0,
    ErrorState,
    ReadyToLoadingState,
    CompleteState
};

@interface ProgressCell : UITableViewCell

- (void)setLoadingState:(State)state;

@end
