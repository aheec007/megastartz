//
//  CommentCell.m
//  tz
//
//  Created by alex on 16.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import "CommentCell.h"

@interface CommentCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;
@end

@implementation CommentCell

- (void)setComment:(Comment *)comment
{
    self.nameLabel.text = comment.name;
    self.emailLabel.text = comment.email;
    self.bodyLabel.text = comment.body;
}

@end
