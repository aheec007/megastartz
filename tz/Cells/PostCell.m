//
//  PostCell.m
//  tz
//
//  Created by alex on 15.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import "PostCell.h"

@interface PostCell()

@property (weak, nonatomic) IBOutlet UILabel *postTitleCell;
@property (weak, nonatomic) IBOutlet UILabel *postBodyCell;

@end

@implementation PostCell

- (void)setPost:(Post *)post
{
    self.postTitleCell.text = post.title;
    self.postBodyCell.text = post.body;
}

@end
