//
//  PostCell.h
//  tz
//
//  Created by alex on 15.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@interface PostCell : UITableViewCell

- (void)setPost:(Post *)post;

@end
