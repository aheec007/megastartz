//
//  Request.m
//  tz
//
//  Created by alex on 16.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import "Request.h"
#import <AFNetworking/AFNetworking.h>
#import "Post.h"
#import "Comment.h"
#import "Constants.h"

@implementation Request

- (AFURLSessionManager *)getManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    return [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
}

- (void)loadPortionOfPosts:(NSInteger)start completionHandler:(void (^)( NSArray *, NSError *))handler
{
    NSDictionary *parameters = [NSDictionary dictionaryWithObjects:@[@(start), @(kPageSize)] forKeys:@[kStart, kLimit]];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", kBasePath, kPosts];
    NSURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:kHTTPGET
                                                                          URLString:urlString
                                                                         parameters:parameters error:nil];
    
    AFURLSessionManager *manager = [self getManager];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, NSArray *responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            handler(nil, error);
        } else {
            NSMutableArray *postsTemp = [NSMutableArray arrayWithCapacity:responseObject.count];
            for (NSDictionary *dict in responseObject){
                [postsTemp addObject:[[Post alloc] initWithDictionary:dict]];
            }
            NSLog(@"%@", postsTemp);
            NSArray *posts = [NSArray arrayWithArray:postsTemp];
            handler(posts, nil);
        }
        
    }];
    [dataTask resume];
}

- (void)loadComments:(Post *)post completionHandler:(void (^)(NSArray *, NSError *))handler
{
    NSString *base = [NSString stringWithFormat:@"%@%@/", kBasePath, kPosts];
    NSString *urlString = [NSString stringWithFormat:@"%@%d/%@/", base, post.uid.intValue, kComments];
    NSURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:kHTTPGET URLString:urlString parameters:nil error:nil];
    
    AFURLSessionManager *manager = [self getManager];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, NSArray *responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            handler(nil, error);
        } else {
            
            NSMutableArray *commentsTemp = [NSMutableArray arrayWithCapacity:responseObject.count];
            for (NSDictionary *dict in responseObject){
                [commentsTemp addObject:[[Comment alloc] initWithDictionary:dict]];
            }
            NSLog(@"%@", commentsTemp);
            NSArray *comments = [NSArray arrayWithArray:commentsTemp];
            handler(comments, nil);
        }
    }];
    [dataTask resume];
}

@end
