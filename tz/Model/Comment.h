//
//  Comment.h
//  tz
//
//  Created by alex on 16.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject
@property (nonatomic, strong) NSNumber *uid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *body;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
