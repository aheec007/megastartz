//
//  Comment.m
//  tz
//
//  Created by alex on 16.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import "Comment.h"

@implementation Comment

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.uid = [dictionary objectForKey:@"id"];
        self.name = [dictionary objectForKey:@"name"];
        self.email = [dictionary objectForKey:@"email"];
        self.body = [dictionary objectForKey:@"body"];
    }
    
    return self;
}

- (NSString *)description
{
    NSArray *components = @[@"objectId:", self.uid.stringValue, @"email:", self.email];
    return [components componentsJoinedByString:@" "];
}
@end
