//
//  Post.m
//  tz
//
//  Created by alex on 14.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import "Post.h"

@implementation Post

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        self.uid = [dictionary objectForKey:@"id"];
        self.title = [dictionary objectForKey:@"title"];
        self.body = [dictionary objectForKey:@"body"];
    }
    
    return self;
}

- (NSString *)description
{
    NSArray *components = @[@"objectId:", self.uid.stringValue, @"title:", self.title];
    return [components componentsJoinedByString:@" "];
}

@end
