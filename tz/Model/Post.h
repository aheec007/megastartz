//
//  Post.h
//  tz
//
//  Created by alex on 14.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Comment;

@interface Post : NSObject

@property (nonatomic, strong) NSNumber *uid;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) NSArray<Comment *> *comments;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
