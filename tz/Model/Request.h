//
//  Request.h
//  tz
//
//  Created by alex on 16.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Post;

@interface Request : NSObject

- (void)loadPortionOfPosts:(NSInteger)start completionHandler:(void (^)( NSArray *, NSError *))handler;
- (void)loadComments:(Post *)post completionHandler:(void (^)(NSArray *, NSError *))handler;

@end

