//
//  ViewController.m
//  tz
//
//  Created by alex on 14.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import "ViewController.h"
#import "Request.h"
#import "Post.h"
#import "PostCell.h"
#import "ProgressCell.h"
#import "CommentsViewController.h"
#import "Constants.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray<Post *> *posts;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property State loadingState;
@property (nonatomic, strong) Request *request;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Posts";
    
    self.posts = @[];
    self.loadingState = ReadyToLoadingState;
    self.table.rowHeight = UITableViewAutomaticDimension;
    self.table.estimatedRowHeight = 100.f;
    UINib *postCellnib = [UINib nibWithNibName:kPostCell bundle:nil];
    [self.table registerNib:postCellnib forCellReuseIdentifier:kPostCell];
    
    UINib *progressNib = [UINib nibWithNibName:kProgressCell bundle:nil];
    [self.table registerNib:progressNib forCellReuseIdentifier:kProgressCell];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.table deselectRowAtIndexPath:self.table.indexPathForSelectedRow animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CommentsViewController *destVC = (CommentsViewController *)segue.destinationViewController;
    destVC.post = self.posts[self.table.indexPathForSelectedRow.row];
}

- (void)loadPortionOfPosts:(NSInteger)start
{
    if(self.loadingState == ReadyToLoadingState){
        self.loadingState = LoadingState;
        
        ViewController __weak *weak_self = self;
        self.request = [[Request alloc] init];
        [self.request loadPortionOfPosts:start completionHandler:^(NSArray *posts, NSError *error) {
            if (error) {
                weak_self.loadingState = ErrorState;
            } else {
                weak_self.loadingState = ReadyToLoadingState;
                if (posts.count == 0){
                    weak_self.loadingState = CompleteState;
                }
                
                weak_self.posts = [weak_self.posts arrayByAddingObjectsFromArray:posts];
            }
            [weak_self.table reloadData];
        }];
    }
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.posts.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == self.posts.count){
        ProgressCell *cell = [tableView dequeueReusableCellWithIdentifier:kProgressCell];
        [self loadPortionOfPosts:self.posts.count];
        [cell setLoadingState:self.loadingState];
        return cell;
    }
        
    PostCell *cell = [tableView dequeueReusableCellWithIdentifier:kPostCell];
    [cell setPost: self.posts[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == self.posts.count){
        if(self.loadingState != LoadingState){
            self.loadingState = ReadyToLoadingState;
            [self.table reloadData];
        }
    } else {
        [self performSegueWithIdentifier:@"toComments" sender:nil];
    }
 }

@end
