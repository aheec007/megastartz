//
//  CommentsViewController.m
//  tz
//
//  Created by alex on 15.12.2017.
//  Copyright © 2017 Alex Home Company. All rights reserved.
//

#import "CommentsViewController.h"
#import "Request.h"
#import "PostCell.h"
#import "Post.h"
#import "ProgressCell.h"
#import "CommentCell.h"
#import "Constants.h"

@interface CommentsViewController ()

@property (nonatomic, strong) NSArray<Comment *> *comments;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property State loadingState;
@property (nonatomic, strong) Request *request;

@end

@implementation CommentsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Comments";
    
    self.table.rowHeight = UITableViewAutomaticDimension;
    self.table.estimatedRowHeight = 100.f;
    UINib *postCellnib = [UINib nibWithNibName:kPostCell bundle:nil];
    [self.table registerNib:postCellnib forCellReuseIdentifier:kPostCell];
    
    UINib *progressNib = [UINib nibWithNibName:kProgressCell bundle:nil];
    [self.table registerNib:progressNib forCellReuseIdentifier:kProgressCell];

    self.comments = @[];
    if(self.post.comments.count){
        self.comments = self.post.comments;
    }
    
    [self loadComments];
    self.loadingState = LoadingState;
}

- (void)loadComments
{
    self.request = [[Request alloc] init];
    [self.request loadComments:self.post completionHandler:^(NSArray *comments, NSError *error) {
        if (error) {
            self.loadingState = ErrorState;
        } else {
            self.loadingState = CompleteState;
            self.comments = [NSArray arrayWithArray:comments];
            self.post.comments = self.comments;
        }
        [self.table reloadData];
    }];
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.comments.count + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0){
        PostCell *cell = [tableView dequeueReusableCellWithIdentifier:kPostCell];
        [cell setPost: self.post];
        
        return cell;
    }
    
    if(indexPath.row == self.comments.count + 1){
        ProgressCell *cell = [tableView dequeueReusableCellWithIdentifier:kProgressCell];
        [cell setLoadingState:self.loadingState];
        return cell;
    }
    
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:kCommentCell];
    [cell setComment:self.comments[indexPath.row - 1]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == self.comments.count + 1){
        if(self.loadingState != LoadingState){
            self.loadingState = ReadyToLoadingState;
            [self.table reloadData];
            [self loadComments];
        }
    }
}

@end
